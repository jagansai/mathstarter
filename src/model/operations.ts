import { TData } from "../common/utils";

export enum OperationType {
    Add = 'Addition',
    Subtract = 'Subtract',
    Multiply = 'Multiply',
    Division = 'Division'
}


export const blue = "#0095ff"
export const red = "#FF0000"
export const green = "#008000"
export const yellow = '#FFB300'

export class LoadProgress {
    progressColor: string = red
    constructor(public loadProgress: number = 0, public loadProgressStr: string = '', public attempts: string = '', private op: Operations) {
    }

    checkResponse() {
        setInterval(() => {
            this.loadProgress++;
            let minutes = Math.floor(this.loadProgress / 60);
            let seconds = this.loadProgress - minutes * 60;
            this.loadProgressStr = `${this.str_pad_left(minutes, '0', 2)}:${this.str_pad_left(seconds, '0', 2)} ${minutes === 0 ? 'secs' : 'min'}`
            this.attempts = `${this.op.problems.right_answers} / ${this.op.problems.data.length}`
            this.progressColor = this.op.problems.right_answers >= (this.op.problems.data.length / 2) ? green : 
                                                                                                              this.op.problems.right_answers > 0 ? yellow 
                                                                                                                                                 : red
            
        }, 1000);
    }

    str_pad_left(str: number, pad: string, len: number) {
        return (new Array(len + 1).join(pad) + str).slice(-len);
    }
}

export class Problems {
    data: TData[] = []
    right_answers = 0
    constructor(private max_questions: number, private max_value: number, private opType: OperationType, private operationSymbol: Map<OperationType, String>) {
        const symbol = this.operationSymbol.get(this.opType)
        for (let num = 0; num <= this.max_questions; ++num) {
            let [lhs, rhs] = this.invertValues((Math.floor(Math.random() * this.max_value) + 10), (Math.floor(Math.random() * this.max_value) + 1) - 1)
            this.data.push({
                left: lhs, right: rhs, opType: symbol,
                itemColor: blue, result: '',
                isAnswerCorrect: false
            })
        }
    }

    invertValues(lhs: number, rhs: number) {
        switch (this.opType) {
            case OperationType.Add:
                return [lhs, rhs]
            case OperationType.Subtract:
                return lhs < rhs ? [rhs, lhs] : [lhs, rhs]
            default:
                return [lhs, rhs]
        }
    }

}

export class Operations {
    items: string[] = []
    opTypeString = ''
    problems: Problems

    constructor(public operationType: OperationType) {     
        this.opTypeString = this.operationType.toString()
        for (var num = 0; num <= 9; ++num) {
            this.items.push(String(num))
        }
        this.items.push("Backspace")
    }

    getMaxValue() {
        switch (this.operationType) {
            case OperationType.Add:
                return 20
            case OperationType.Subtract:
                return 10
            default:
                return 10
        }
    }



    getOperationSymbol() {
        let map = new Map<OperationType, string>()
        map.set(OperationType.Add, ' + ')
        map.set(OperationType.Subtract, ' - ')
        map.set(OperationType.Multiply, ' * ')
        map.set(OperationType.Division, ' \ ')
        return map
    }

    numberClick(num: string, index: number) {
        if (this.problems.data[index].result.length > 10)
            return
        if (this.problems.data[index].result == '' && num == '0')
            return
        if (num == 'Backspace')
            this.problems.data[index].result = this.erase(this.problems.data[index].result)
        else
            this.problems.data[index].result += num

        const desiredValue = this.doOpertation(this.problems.data[index].left, this.problems.data[index].right)
        this.problems.data[index].itemColor = (desiredValue === Number(this.problems.data[index].result) ? green : red)
        this.problems.data[index].isAnswerCorrect = (desiredValue === Number(this.problems.data[index].result))
        this.problems.right_answers = this.problems.data.map(a => a.isAnswerCorrect ? Number(1) : Number(0)).reduce((a, b) => a + b)
    }

    doOpertation(lhs: number, rhs: number): number {
        switch (this.operationType) {
            case OperationType.Add:
                return lhs + rhs
            case OperationType.Subtract:
                return lhs - rhs
            default:
                return 0
        }
    }

    erase(str: string): string {
        if (str == '')
            return ''
        return str.substring(0, str.length - 1)
    }
}