import { Component, Renderer } from '@angular/core';
import { NavController} from 'ionic-angular';
import { Operations, OperationType, LoadProgress, Problems } from '../../model/operations';

/**
 * Generated class for the SubtractPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-subtract',
  templateUrl: 'subtract.html',
})
export class SubtractPage extends Operations {
  loadProgress : LoadProgress
  constructor(public navCtrl: NavController, public renderer: Renderer) {
    super(OperationType.Subtract)  
    this.loadProgress = new LoadProgress(0, '', '', this)    
    this.problems = new Problems(15, this.getMaxValue(), OperationType.Subtract, this.getOperationSymbol())  
  }

  ionViewDidEnter() {
    this.loadProgress.checkResponse()
  }

}
