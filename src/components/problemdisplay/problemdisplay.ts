import { Component, ViewChild, OnInit, Input, Renderer } from '@angular/core';

/**
 * Generated class for the ProblemDisplayComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'problemdisplay',
  templateUrl: 'problemdisplay.html'
})


export class ProblemDisplayComponent implements OnInit {
  accordionExapanded = false;
  @ViewChild("cc") cardContent: any;
  @Input('left') left: string;
  @Input('right') right: string;
  @Input('opType') opType: string;
  @Input('result') result: string;
  @Input('headerColor') headerColor: string;

  icon: string = "arrow-round-forward";
  md: string = "md-arrow-round-forward"

  constructor(public renderer: Renderer) {
  }

  ngOnInit(): void {
  }

  toggleAccordion(num: String) {
     if (this.accordionExapanded) {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "0px 16px");

    } else {
      this.renderer.setElementStyle(this.cardContent.nativeElement, "max-height", "1500px");
      this.renderer.setElementStyle(this.cardContent.nativeElement, "padding", "13px 16px");

    }

    this.accordionExapanded = !this.accordionExapanded;
    this.icon = this.icon == "arrow-round-forward" ? "arrow-round-down" : "arrow-round-forward";
    this.md = this.md == "md-arrow-round-forward" ? "md-arrow-round-down" : "md-arrow-round-forward"
  }
}
