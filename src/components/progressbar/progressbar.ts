import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the ProgressbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'progressbar',
  templateUrl: 'progressbar.html'
})
export class ProgressbarComponent {

  @Input('progress') progress : any;
  @Input('title') title : any;
  @Input('attempts') attempts : any;
  @Input('headerColor') headerColor: string;
  fgColor = '#0000'
 
  constructor(public navCtrl: NavController) {    
  }

  goToHomePage() {
    this.navCtrl.setRoot(HomePage)
  }
}
