import {Component, Renderer} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Operations, OperationType, LoadProgress, Problems} from '../../model/operations';

@Component({
    selector: 'page-add',
    templateUrl: 'add.html'
})
export class AddPage extends Operations {

    loadProgress: LoadProgress;

    constructor(public navCtrl: NavController, public renderer: Renderer) {
        super(OperationType.Add);
        this.loadProgress = new LoadProgress(0, '', '', this);
        this.problems = new Problems(15, this.getMaxValue(), OperationType.Add, this.getOperationSymbol())
    }

    ionViewDidEnter() {
        this.loadProgress.checkResponse()
    }
}
