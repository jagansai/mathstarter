

export type TData = 
{ left: number, 
  right: number, 
  opType: String, 
  itemColor: string,
  result: string,
  isAnswerCorrect: boolean
}

export class Utils {
    // static getTaskPart(data: TData[], index: number): string {
    //     let idx = data[index].task.indexOf('=')
    //     return idx === -1 ? data[index].task : data[index].task.substring(0, idx)
    // }

    // static getLhsAndRhs(data: TData[], index: number, searchFor: string): [number, number] {
    //     let taskPart = Utils.getTaskPart(data, index)
    //     let idx = taskPart.indexOf(searchFor)
    //     let lhs = taskPart.substring(0, idx)
    //     let rhs = taskPart.substring(idx + 3)
    //     return [Number(lhs), Number(rhs)]
    // }
}