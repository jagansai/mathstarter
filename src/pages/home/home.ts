import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { OperationType } from "../../model/operations";
import { AddPage } from "../add/add";
import { SubtractPage } from '../subtract/subtract';
//import {NativePageTransitions, NativeTransitionOptions} from '@ionic-native/native-page-transitions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  operations: OperationType[] = [];
  opPageMap: Map<OperationType, any> = new Map<OperationType, any>();
  
  constructor(public navCtrl: NavController) {
    this.operations.push(OperationType.Add);
    this.operations.push(OperationType.Subtract);

    // map the operationType with Page.
    this.opPageMap.set(OperationType.Add, AddPage);
    this.opPageMap.set(OperationType.Subtract, SubtractPage)    
  }

  goToProblem(op: OperationType) {
    /*let options : NativeTransitionOptions = {
        direction: 'left',
        duration: 400,
        slowdownfactor: -1,
        androiddelay: 50
    };
    this.nativePageTransitions.slide(options);*/
    this.navCtrl.setRoot(this.opPageMap.get(op))
  }

}
