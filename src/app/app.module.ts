import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {AddPage} from '../pages/add/add'
import {SubtractPage} from "../pages/subtract/subtract";


import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';


import {ProblemDisplayComponent} from '../components/problemdisplay/problemdisplay';
import {ProgressbarComponent} from '../components/progressbar/progressbar';
//import {NativePageTransitions} from "@ionic-native/native-page-transitions";


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        AddPage,
        SubtractPage,
        ProblemDisplayComponent,
        ProgressbarComponent

    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        AddPage,
        SubtractPage
        // AccordionListPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
